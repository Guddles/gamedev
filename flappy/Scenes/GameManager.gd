extends Node


@onready var bird = $"../Bird" as Bird
@onready var pipe_spawner = $"../PipeSpawner" as PipeSpawner
@onready var ground = $"../Ground" as Ground
@onready var ui = $"../UI" as UI

var points = 0

func _ready():
	
	bird.game_started.connect(on_game_started)
	pipe_spawner.bird_crashed.connect(game_end)
	pipe_spawner.point_scored.connect(on_point_scored)
	

func on_game_started():
	ui.on_main_menu()
	pipe_spawner.start_spawning_pipes()
	
	
func game_end():
	#ground.stop()
	bird.kill()
	pipe_spawner.stop()
	
	ui.on_game_over()
	

func on_point_scored():
	points +=1
	ui.update_points(points)
