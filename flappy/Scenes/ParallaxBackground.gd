extends ParallaxBackground

#class_name Ground
@export var scrolling_speed = 5
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	scroll_base_offset.x -= scrolling_speed + delta

func stop():
	scrolling_speed = 0
