extends CanvasLayer
class_name UI
@onready var points_label = $MarginContainer/PointsLabel
@onready var game_over_box = $MarginContainer/GameOverBox
@onready var game_menu_box = $MarginContainer/GameMenuBox

func _ready():
	points_label.text = "%d" % 0
	
func update_points(points: int):
	points_label.text = "%d" % points

func on_game_over():
	game_over_box.visible = true

func on_main_menu():
	game_menu_box.visible = false

func _on_button_pressed():
	get_tree().reload_current_scene()


func _on_texture_button_pressed():
	get_tree().reload_current_scene()
