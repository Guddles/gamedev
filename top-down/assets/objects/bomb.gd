extends Area2D

const time_to_explode = 2.0
const damage = 3



# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.



var targets = []
var timer = 0.0
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	timer += delta
	if timer >= time_to_explode:
		for i in range(targets.size()):
			targets[i].take_damage(damage)
		queue_free()



func _on_body_entered(body):
	if body.is_in_group("can_be_damaged"):
		targets.push_back(body)



func _on_body_exited(body):
	if body.is_in_group("can_be_damaged"):
		for i in range(targets.size() -1, -1, -1):
			if targets[i] == body:
				targets.remove_at(i)
