extends CharacterBody2D

@onready var animation = $animation

const speed = 50
const damage = 1



func _ready():
	pass



func _process(delta):
	pass



var cur_state = "idle"
var cur_direction = "down"
var target = null
var target_direction = Vector2(0, 0)
var attack_flag = false
func _physics_process(delta):
	if Engine.time_scale > 0:
		if cur_state != "dead":
			if (cur_state == "follow") and (target):
				target_direction = self.global_position.direction_to(target.global_position)
				if abs(target_direction.x) > abs(target_direction.y):
					if target_direction.x > 0:
						animation.flip_h = true
						animation.play("left")
					else:
						animation.flip_h = false
						animation.play("left")
				else:
					if target_direction.y > 0:
						animation.play("down")
					else:
						animation.flip_h = false
						animation.play("up")
				self.global_position += speed*delta*target_direction
			if (attack_flag) and (target):
				target.take_damage(damage)



func take_damage(dmg):
	if (cur_state != "dead"):
		get_killed()



func get_killed():
	cur_state = "dead"
	animation.modulate = Color.BLACK
	animation.stop()
	self.set_collision_layer_value(32, true)
	self.set_collision_layer_value(3, false)
	self.set_collision_mask_value(2, false)



func _on_detect_body_entered(body):
	if (cur_state == "idle") and (body.name == "hero"):
		target = body
		cur_state = "follow"



func _on_attack_body_entered(body):
	if body.name == "hero":
		attack_flag = true



func _on_attack_body_exited(body):
	if body.name == "hero":
		attack_flag = false


func _on_screen_exited():
	queue_free()
