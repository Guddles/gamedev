extends Node2D

const GHOST = preload("res://assets/characters/enemies/ghost/ghost.tscn")

@onready var pause_menu = $camera/ui/pause
@onready var player = get_tree().get_root().get_node("level/hero")
@onready var camera = get_tree().get_root().get_node("level/camera")

var paused
var ghosts_spawn = null
# Called when the node enters the scene tree for the first time.
func _ready():
	Engine.time_scale = 1
	paused = false
	pause_menu.hide()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("pause"):
		pauseMenu()



func pauseMenu():
	if paused:
		pause_menu.hide()
		Engine.time_scale = 1
	else:
		pause_menu.show()
		Engine.time_scale = 0
	paused = !paused



var direction = null
func changeScreen():
	Engine.time_scale = 0
	if direction == "up":
		player.global_position.y -= 64
		camera.global_position.y -= 12*32
	elif direction == "down":
		player.global_position.y += 64
		camera.global_position.y += 12*32
	
	if direction:
		if ghosts_spawn and GHOST:
			for pos in ghosts_spawn.size():
				var ghost = GHOST.instantiate()
				ghost.global_position = ghosts_spawn[pos]
				self.add_child(ghost)
		direction = null
	Engine.time_scale = 1



var scene = null
var hero_spawn = null
func changeScene():
	Engine.time_scale = 0
	if scene:
		get_tree().change_scene_to_packed(scene)
		scene = null
	Engine.time_scale = 1
