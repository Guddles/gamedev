require("vector")
require("game")
require("menu")
require("page")
require("menu_item")

skins = {"default", "blood", "orange", "tape", "army"}
menu_imgs = {}
function change_skin(flag)
    if type(flag) == "boolean" then
        if flag then
            skin_num = skin_num + 1
            if skin_num > #skins then
                skin_num = 1
            end
        else
            skin_num = skin_num - 1
            if skin_num < 1 then
                skin_num = #skins
            end
        end
    elseif type(flag) == "number" then
        if (flag >= #skins+1) or (flag < 1) then
            skin_num = 1
        else
            skin_num = math.floor(flag)
        end
    end

    arrLImg =       love.graphics.newImage("resources/" .. skins[skin_num] .. "/arrL.png")
    arrRImg =       love.graphics.newImage("resources/" .. skins[skin_num] .. "/arrR.png")
    ballImg =       love.graphics.newImage("resources/" .. skins[skin_num] .. "/ball.png")
    boxImg =        love.graphics.newImage("resources/" .. skins[skin_num] .. "/box.png")
    digitsImg =     love.graphics.newImage("resources/" .. skins[skin_num] .. "/digits.png")
    endImg =        love.graphics.newImage("resources/" .. skins[skin_num] .. "/end.png")
    p1Img =         love.graphics.newImage("resources/" .. skins[skin_num] .. "/p1.png")
    p2Img =         love.graphics.newImage("resources/" .. skins[skin_num] .. "/p2.png")
    mainscreenImg = love.graphics.newImage("resources/" .. skins[skin_num] .. "/mainscreen.png")
    menuscreenImg = love.graphics.newImage("resources/" .. skins[skin_num] .. "/menuscreen.png")
    playerImg =     love.graphics.newImage("resources/" .. skins[skin_num] .. "/player.png")
    titleImg =      love.graphics.newImage("resources/" .. skins[skin_num] .. "/title.png")
    volumebarImg =  love.graphics.newImage("resources/" .. skins[skin_num] .. "/volumebar.png")
    volumedotImg =  love.graphics.newImage("resources/" .. skins[skin_num] .. "/volumedot.png")
    wonImg =        love.graphics.newImage("resources/" .. skins[skin_num] .. "/won.png")
    menu_imgs["exitImg"] =      love.graphics.newImage("resources/" .. skins[skin_num] .. "/exit.png")
    menu_imgs["optImg"] =       love.graphics.newImage("resources/" .. skins[skin_num] .. "/opt.png")
    menu_imgs["pveImg"] =       love.graphics.newImage("resources/" .. skins[skin_num] .. "/pve.png")
    menu_imgs["pvpImg"] =       love.graphics.newImage("resources/" .. skins[skin_num] .. "/pvp.png")
    menu_imgs["startImg"] =     love.graphics.newImage("resources/" .. skins[skin_num] .. "/start.png")
    menu_imgs["styleImg"] =     love.graphics.newImage("resources/" .. skins[skin_num] .. "/style.png")
    menu_imgs["volumeImg"] =    love.graphics.newImage("resources/" .. skins[skin_num] .. "/volume.png")
    menu_imgs["backImg"] =      love.graphics.newImage("resources/" .. skins[skin_num] .. "/back.png")
end

function change_resolution(x, y)
    width = x
    height = y
    widthScale = width / 960
    heightScale = height / 540
    menu_zone_x = 45 * widthScale
    menu_zone_y = 67 * heightScale
    menu_zone_width = 121 * widthScale
    menu_zone_height = 149 * heightScale
    play_zone_x = 240 * widthScale
    play_zone_y = 67 * heightScale
    play_zone_width = 661 * widthScale
    play_zone_height = 430 * heightScale
    game:change_positions()
    love.window.setMode(width, height, {borderless = true})
end

fullscreen_mode = false
function change_fullscreen(x, y)
    if fullscreen_mode then
        love.window.setMode(x, y, {borderless = true, fullscreen = false})
        fullscreen_mode = false
        widthScale = x / 960
        heightScale = y / 540
        menu_zone_x = 45 * widthScale
        menu_zone_y = 67 * heightScale
        menu_zone_width = 121 * widthScale
        menu_zone_height = 149 * heightScale
        play_zone_x = 240 * widthScale
        play_zone_y = 67 * heightScale
        play_zone_width = 661 * widthScale
        play_zone_height = 430 * heightScale
        game:change_positions()
    else
        love.window.setMode(0, 0, {borderless = true, fullscreen = true})
        fullscreen_mode = true
        local x = love.graphics.getWidth()
        local y = love.graphics.getHeight()
        widthScale = x / 960
        heightScale = y / 540
        menu_zone_x = 45 * widthScale
        menu_zone_y = 67 * heightScale
        menu_zone_width = 121 * widthScale
        menu_zone_height = 149 * heightScale
        play_zone_x = 240 * widthScale
        play_zone_y = 67 * heightScale
        play_zone_width = 661 * widthScale
        play_zone_height = 430 * heightScale
        game:change_positions()
    end
end

function set_volume()
    sounds.background:setVolume(volume/100)
    sounds.bounce:setVolume(volume/100)
    sounds.goal:setVolume(volume/100)
    sounds.selected:setVolume(volume/100)
    sounds.switch:setVolume(volume/100)
end

function love.load()
    love.window.setTitle("Pong")
    resolution_width = 960
    resolution_height = 540
    resolution_mult = 1.2
    widthScale = resolution_width / 960
    heightScale = resolution_height / 540
    menu_zone_x = 45 * widthScale
    menu_zone_y = 67 * heightScale
    menu_zone_width = 121 * widthScale
    menu_zone_height = 149 * heightScale
    play_zone_x = 240 * widthScale
    play_zone_y = 67 * heightScale
    play_zone_width = 661 * widthScale
    play_zone_height = 430 * heightScale
    love.window.setMode(resolution_width, resolution_height, {borderless = true})
    math.randomseed(os.time())
    time_to_update = 1/60
    options_page_flag = false
    volume = 100
    skin_num = 1
    change_skin(skin_num)

    sounds = {}
    sounds.background = love.audio.newSource("resources/sounds/background.wav", "stream")
    sounds.bounce = love.audio.newSource("resources/sounds/bounce.wav", "static")
    sounds.goal = love.audio.newSource("resources/sounds/goal.wav", "static")
    sounds.selected = love.audio.newSource("resources/sounds/selected.wav", "static")
    sounds.switch = love.audio.newSource("resources/sounds/switch.wav", "static")
    sounds.background:setLooping(true)
    sounds.background:play()

    menuscreenImg_quads = {}
    menu_frames = menuscreenImg:getWidth() / 121
	for i=0, menu_frames-1 do
		table.insert(menuscreenImg_quads, love.graphics.newQuad(i * 121, 0, 121, menuscreenImg:getHeight(), menuscreenImg:getWidth(),
                        menuscreenImg:getHeight()))
	end

    digitsImg_quads = {}
    digits_frames = 10
    digits_frame_width = digitsImg:getWidth()/4
    digits_frame_height = digitsImg:getHeight()/3
	for i=0, digits_frames-1 do
		table.insert(digitsImg_quads, love.graphics.newQuad(i%4 * digits_frame_width, math.floor(i/4)*digits_frame_height,
                        digits_frame_width, digits_frame_height, digitsImg:getWidth(), digitsImg:getHeight()))
	end

    game = Game:create()
    game:create_ball()
    game:create_players()

    menu = Menu:create()
    main_page = Page:create()
    modes_page = Page:create()
    options_page = Page:create()

    main_page:add_button(MenuItem:create("startImg", function() modes_page:load() sounds.selected:stop() sounds.selected:play() end))
    main_page:add_button(MenuItem:create("optImg", function() options_page:load() options_page_flag=true sounds.selected:stop() sounds.selected:play() end))
    main_page:add_button(MenuItem:create("exitImg", function() love.event.quit() end))

    modes_page:add_button(MenuItem:create("pvpImg", function() game:start(false) sounds.selected:stop() sounds.selected:play() end))
    modes_page:add_button(MenuItem:create("pveImg",function() game:start(true) sounds.selected:stop() sounds.selected:play() end))
    modes_page:add_button(MenuItem:create("backImg", function() main_page:load() sounds.selected:stop() sounds.selected:play() end))

    options_page:add_button(MenuItem:create("volumeImg", function() end))
    options_page:add_button(MenuItem:create("styleImg", function() end))
    options_page:add_button(MenuItem:create("backImg", function() main_page:load() options_page_flag=false sounds.selected:stop() sounds.selected:play() end))
    main_page:load()
end

timer = 0
timer_for_quads = 0
function love.update(dt)
    timer = timer + dt
    timer_for_quads = timer_for_quads + dt*10
    menu:update(dt)
    game:update(dt)
end

function love.draw()
    love.graphics.draw(mainscreenImg, play_zone_x, play_zone_y, 0, widthScale, heightScale)
    love.graphics.draw(menuscreenImg, menuscreenImg_quads[math.floor(timer_for_quads) % menu_frames + 1], menu_zone_x, menu_zone_y,
                        0, widthScale, heightScale)
    menu:draw()
    game:draw()

    if options_page_flag then
        love.graphics.draw(arrLImg, menu_zone_x+menu_zone_width/6-arrLImg:getWidth()*widthScale/2,
                                        menu_zone_y+menu_zone_height/4-arrLImg:getHeight()*heightScale/2, 0, widthScale, heightScale)
        love.graphics.draw(arrRImg, menu_zone_x+menu_zone_width/6*5-arrRImg:getWidth()*widthScale/2,
                                        menu_zone_y+menu_zone_height/4-arrRImg:getHeight()*heightScale/2, 0, widthScale, heightScale)

        love.graphics.draw(volumebarImg, menu_zone_x+menu_zone_width/2-volumebarImg:getWidth()*widthScale/2,
                                        menu_zone_y+menu_zone_height/4+arrRImg:getHeight()*heightScale/2+volumebarImg:getHeight()*heightScale/2,
                                        0, widthScale, heightScale)
        love.graphics.draw(volumedotImg, menu_zone_x+(menu_zone_width/2-volumebarImg:getWidth()*widthScale/2)+
                                        ((volumebarImg:getWidth()-volumedotImg:getWidth())*widthScale/100*volume),
                                        menu_zone_y+menu_zone_height/4+arrRImg:getHeight()*heightScale/2, 0, widthScale, heightScale)

        love.graphics.draw(arrLImg, menu_zone_x+menu_zone_width/6-arrLImg:getWidth()*widthScale/2,
                                        menu_zone_y+menu_zone_height/4*2-arrLImg:getHeight()*heightScale/2, 0, widthScale, heightScale)
        love.graphics.draw(arrRImg, menu_zone_x+menu_zone_width/6*5-arrRImg:getWidth()*widthScale/2,
                                        menu_zone_y+menu_zone_height/4*2-arrRImg:getHeight()*heightScale/2, 0, widthScale, heightScale)
    end

    love.graphics.draw(boxImg, 0, 0, 0, widthScale, heightScale)

    -- --дебажная инфа
    -- love.graphics.setColor(1, 0, 0)
    -- for i = 1, #game.ball.points do
    --     love.graphics.circle("fill", game.ball.points[i].x, game.ball.points[i].y, 2)
    --     love.graphics.print("point ".. tostring(i) .. ": " .. tostring(game.ball.points[i].x) .. " " .. tostring(game.ball.points[i].y), 10, 10+i*10)
    -- end
    -- love.graphics.line(game.player2.x, game.player2.y, game.player2.x + game.player2.sizes["width"], game.player2.y+game.player2.sizes["height"])
    -- love.graphics.line(game.player1.x, game.player1.y, game.player1.x + game.player1.sizes["width"], game.player1.y+game.player1.sizes["height"])
    -- love.graphics.print("ball angle: " .. tostring(game.ball.angle), 10, 10)
    -- love.graphics.setColor(1, 1, 1)
end

function love.keypressed(key)
    if key == 'escape' then
        if game.mode then
            game:escapepressed()
        else
            menu:escapepressed()
        end
    elseif (key == 'space') or (key == 'return') then
        menu:returnpressed()
        if game.gamend then
            game:spacepressed()
        end
    -- elseif key == 'e' then
    --     love.event.quit()
    elseif (key == 'w') or (key == 'up') then
        menu:uppressed()
    elseif (key == 's') or (key == 'down') then
        menu:downpressed()
    elseif (key == 'a') or (key == 'left') then
        menu:leftpressed()
    elseif (key == 'd') or (key == 'right') then
        menu:rightpressed()
    end
    if not game.mode then
        if (key == '1') then
            resolution_width = resolution_width/resolution_mult
            if resolution_width < 960 then
                resolution_width = 960
            else
                change_resolution(resolution_width, resolution_height)
            end
        elseif (key == '2') then
            resolution_width = resolution_width*resolution_mult
            change_resolution(resolution_width, resolution_height)
        elseif (key == '3') then
            resolution_height = resolution_height/resolution_mult
            if resolution_height < 540 then
                resolution_height = 540
            else
                change_resolution(resolution_width, resolution_height)
            end
        elseif (key == '4') then
            resolution_height = resolution_height*resolution_mult
            change_resolution(resolution_width, resolution_height)
        elseif (key == 'f') then
            change_fullscreen(resolution_width, resolution_height)
        end
    end
end
