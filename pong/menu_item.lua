MenuItem = {}
MenuItem.__index = MenuItem

function MenuItem:create(image_name, action)
    local menuItem = {}
    setmetatable(menuItem, MenuItem)

    menuItem.image_name = image_name
    menuItem.action = action

    return menuItem
end

function MenuItem:draw(chk, x, y)
    local img_ = menu_imgs[self.image_name]
    local x_
    local y_
    if x then
        x_ = x
    else
        x_ = menu_zone_x + menu_zone_width/2 - img_:getWidth()*widthScale/2
    end
    if y then
        y_ = y
    else
        y_ = menu_zone_y + menu_zone_height/4 - img_:getHeight()*heightScale/2
    end
    if chk then
        love.graphics.setColor(0, 0, 255)
        love.graphics.rectangle("fill", x_, y_, img_:getWidth()*widthScale, img_:getHeight()*heightScale)
    end
    love.graphics.setColor(255, 255, 255) -- белый цвет для изображения элемента меню
    love.graphics.draw(img_, x_, y_, 0, widthScale, heightScale)
end

function MenuItem:do_action()
    self.action()
end