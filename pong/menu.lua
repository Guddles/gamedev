require("menu_item")

Menu = {}
Menu.__index = Menu
function Menu:create()
    local menu = {}
    setmetatable(menu, Menu)
    menu.items = {}
    menu.selectedIndex = 1
    menu.interact = true
    menu.down_timer = 0
    menu.time_to_action = 0.2
    return menu
end

function Menu:update(dt)
    if self.interact then
        self.down_timer = self.down_timer + dt
        if ((love.keyboard.isDown("up")) or (love.keyboard.isDown("w"))) and (self.down_timer > self.time_to_action) then
            self.down_timer = 0
            self:uppressed()
        elseif ((love.keyboard.isDown("down")) or (love.keyboard.isDown("s"))) and (self.down_timer > self.time_to_action) then
            self.down_timer = 0
            self:downpressed()
        elseif ((love.keyboard.isDown("left")) or (love.keyboard.isDown("a"))) and (self.down_timer > self.time_to_action) then
            self.down_timer = 0
            self:leftpressed()
        elseif ((love.keyboard.isDown("right")) or (love.keyboard.isDown("d"))) and (self.down_timer > self.time_to_action) then
            self.down_timer = 0
            self:rightpressed()
        end
    end
end

function Menu:draw()
    if self.interact then
        for i, item in ipairs(self.items) do
            if (i == menu.selectedIndex) and (self.interact) then
                item:draw(true, false, menu_zone_y+menu_zone_height/(#self.items+1)*i-menu_imgs[item.image_name]:getHeight()/2)
            else
                item:draw(false, false, menu_zone_y+menu_zone_height/(#self.items+1)*i-menu_imgs[item.image_name]:getHeight()/2)
            end
        end
    end
end

function Menu:clear()
    self.items = nil
end

function Menu:uppressed()
    self.down_timer = 0
    if self.interact then
        sounds.switch:stop()
        sounds.switch:play()
        self.selectedIndex = self.selectedIndex - 1
        if self.selectedIndex < 1 then
            self.selectedIndex = #self.items
        end
    end
end

function Menu:downpressed()
    self.down_timer = 0
    if self.interact then
        sounds.switch:stop()
        sounds.switch:play()
        self.selectedIndex = self.selectedIndex + 1
        if self.selectedIndex > #self.items then
            self.selectedIndex = 1
        end
    end
end

function Menu:leftpressed()
    self.down_timer = 0
    if options_page_flag then
        if self.selectedIndex == 1 then
            volume = volume-5
            if volume < 0 then
                volume = 0
            end
            set_volume()
            sounds.switch:stop()
            sounds.switch:play()
        elseif self.selectedIndex == 2 then
            sounds.switch:stop()
            sounds.switch:play()
            change_skin(false)
        end
    end
end

function Menu:rightpressed()
    self.down_timer = 0
    if options_page_flag then
        if self.selectedIndex == 1 then
            volume = volume+5
            if volume > 100 then
                volume = 100
            end
            set_volume()
            sounds.switch:stop()
            sounds.switch:play()
        elseif self.selectedIndex == 2 then
            sounds.switch:stop()
            sounds.switch:play()
            change_skin(true)
        end
    end
end

function Menu:returnpressed()
    if self.interact then
        self.items[self.selectedIndex]:do_action()
    end
end

function Menu:escapepressed()
    if self.interact then
        self.items[3]:do_action()
    end
end