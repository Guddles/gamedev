require("menu_item")
Page = {}
Page.__index = Page

function Page:create()
    local page = {}
    setmetatable(page, Page)

    page.buttons = {}
    return page
end

function Page:add_button(button)
    table.insert(self.buttons, button)
end

function Page:load()
    menu.items = nil
    menu.items = self.buttons
    menu.selectedIndex = 1
end
