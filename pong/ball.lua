Ball = {}
Ball.__index = Ball

function Ball:create(sizes)
    local ball = {}
    setmetatable(ball, Ball)
    ball.location = Vector:create(play_zone_width/2 + play_zone_x, play_zone_height/2 + play_zone_y)
    ball.velocity = Vector:create(0, 0)
    ball.acceleration = 1.1
    ball.or_sizes = {sizes[1]*widthScale/2, sizes[2]*heightScale/2}
    ball.sizes = {sizes[1]*widthScale/2, sizes[2]*heightScale/2}
    -- ball.or_size = sizes[1]*widthScale
    -- ball.size = sizes[2]*heightScale
    ball.size_speed = 5
    ball.points = {}
    ball.angle = 0
    return ball
end

function Ball:launch(speed_vector)
    self.size = self.or_size
    self.sizes = self.or_sizes
    self.location = Vector:create(play_zone_width/2 + play_zone_x, play_zone_height/2 + play_zone_y)
    turn = math.random() < 0.5
    if turn then
        turn = 1
    else
        turn = -1
    end
    local rot = math.random(-15, 15)
    self.angle = rot
    local x = (speed_vector.x*math.cos(rot*math.pi/180) + speed_vector.y*math.sin(rot*math.pi/180)) * turn
    local y = speed_vector.y*math.cos(rot*math.pi/180) - speed_vector.x*math.sin(rot*math.pi/180)
    self.velocity = Vector:create(x, y)
    return turn
end

function Ball:draw()
    love.graphics.draw(ballImg, self.location.x-ballImg:getWidth()*self.sizes[1]/ballImg:getWidth(),
                        self.location.y-ballImg:getHeight()*self.sizes[2]/ballImg:getHeight(),
                        0, self.sizes[1]/ballImg:getWidth()*2, self.sizes[2]/ballImg:getHeight()*2)
end

function Ball:add_speed()
    self.velocity:mul(self.acceleration)
end

function Ball:change_size()
    self.sizes = {self.sizes[1] - self.size_speed*widthScale, self.sizes[2] - self.size_speed*heightScale}
    if self.sizes[1] < 10*widthScale then
        self.sizes[1] = 10*widthScale
    end
    if self.sizes[2] < 10*heightScale then
        self.sizes[2] = 10*heightScale
    end
end

function Ball:update(dt)
    --self.location:mul(self.velocity)
    self.location = self.location + self.velocity*dt
end

function Ball:check_boundaries()
    if self.location.x > play_zone_width + play_zone_x - self.sizes[1] then
        self.location.x = play_zone_width + play_zone_x - self.sizes[1]
        self.velocity.x = -1*self.velocity.x
        return true
    elseif self.location.x < play_zone_x + self.sizes[1] then
        self.location.x = play_zone_x + self.sizes[1]
        self.velocity.x = -1*self.velocity.x
        return true
    end
    if self.location.y > play_zone_height + play_zone_y - self.sizes[2] then
        self.location.y = play_zone_height + play_zone_y - self.sizes[2]
        self.velocity.y = -1*self.velocity.y
        self.angle = self.angle*-1
        sounds.bounce:stop()
        sounds.bounce:play()
        return false
    elseif self.location.y < play_zone_y + self.sizes[2] then
        self.location.y = play_zone_y + self.sizes[2]
        self.velocity.y = -1*self.velocity.y
        self.angle = self.angle*-1
        sounds.bounce:stop()
        sounds.bounce:play()
        return false
    end
end

function Ball:check_player(player)
    --local rot
    local x
    local y
    local i_ = 4
    for i=0, i_ do
        self.points[i+1] = Vector:create(self.location.x+self.sizes[1]*i/i_*player.side,
                                            self.location.y+(math.sqrt((self.sizes[1]/widthScale)^2-((self.sizes[1]/widthScale)*i/i_)^2))*heightScale)
    end
    for i=1, i_ do
        self.points[i_+1+i] = Vector:create(self.points[i_+1-i].x,
                                            self.location.y-(math.sqrt((self.sizes[1]/widthScale)^2-((math.abs(self.points[i_+1-i].x)-self.location.x)/widthScale)^2))*heightScale)
    end
    for i = 1, #self.points do
        if (self.points[i].y >= player.y) and (self.points[i].y <= player.y+player.sizes["height"]) then
            if (self.points[i].x >= player.x) and (self.points[i].x <= player.x+player.sizes["width"]) then
                self.velocity.x = -1*self.velocity.x
                if (player.velocity.y ~= 0) then
                    if (math.abs(player.velocity.y) == speed) then
                        self.rot = game.speed_rot
                    else
                        self.rot = game.speed_rot*game.boost_rot
                    end
                    if player.velocity.y<0 then
                        new_angle = math.abs(self.angle - self.rot)
                    else
                        new_angle = math.abs(self.angle + self.rot)
                    end
                    if (new_angle > game.max_angle) then
                        self.rot = game.max_angle - math.abs(self.angle)
                    end
                    --по часовой, если player.side = 1
                    if player.velocity.y<0 then
                        x = self.velocity.x*math.cos(self.rot*math.pi/180) + self.velocity.y*math.sin(self.rot*math.pi/180)*player.side
                        y = self.velocity.y*math.cos(self.rot*math.pi/180) - self.velocity.x*math.sin(self.rot*math.pi/180)*player.side
                        self.rot = -1*self.rot
                    else
                        x = self.velocity.x*math.cos(self.rot*math.pi/180) - self.velocity.y*math.sin(self.rot*math.pi/180)*player.side
                        y = self.velocity.y*math.cos(self.rot*math.pi/180) + self.velocity.x*math.sin(self.rot*math.pi/180)*player.side
                    end
                    self.angle = self.angle + self.rot
                    self.velocity.x = x
                    self.velocity.y = y
                end
                if (i ~= math.ceil(#self.points/2)) then
                    local i_ = i-math.ceil(#self.points/2)
                    self.rot = game.speed_rot*game.boost_rot*i_/4*-1
                    new_angle = math.abs(self.angle + self.rot)
                    if (new_angle > game.max_angle) then
                        self.rot = game.max_angle - math.abs(self.angle)
                        if i_>0 then
                            self.rot = self.rot*-1
                        end
                    end
                    self.angle = self.angle + self.rot
                    x = self.velocity.x*math.cos(self.rot*math.pi/180) - self.velocity.y*math.sin(self.rot*math.pi/180)*player.side
                    y = self.velocity.y*math.cos(self.rot*math.pi/180) + self.velocity.x*math.sin(self.rot*math.pi/180)*player.side
                    self.velocity.x = x
                    self.velocity.y = y
                end
                sounds.bounce:stop()
                sounds.bounce:play()
                return true
            end
        end
    end
end

