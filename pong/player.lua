Player = {}
Player.__index = Player

function Player:create(location, sizes, keys, side, ai)
    local player = {}
    setmetatable(player, Player)
    player.location = location
    player.sizes = {}
    player.sizes["width"] = sizes[1]*widthScale
    player.sizes["height"] = sizes[2]*heightScale
    player.velocity = Vector:create(0, 0)
    player.up_key = keys[1]
    player.down_key = keys[2]
    player.boost_key = keys[3]
    player.x = player.location.x - player.sizes["width"]/2
    player.y = player.location.y - player.sizes["height"]/2
    player.side = side
    player.ai = ai
    player.point = math.random(game.ball.location.y-game.ball.or_sizes[2], game.ball.location.y+game.ball.or_sizes[2])
    return player
end

function Player:change_positions(location)
    self.location = location
    self.x = self.location.x - self.sizes["width"]/2
    self.y = self.location.y - self.sizes["height"]/2
end

function Player:control()
    if love.keyboard.isDown(self.boost_key) then
        final_speed = game.speed*game.boost
    else
        final_speed = game.speed
    end
    if love.keyboard.isDown(self.up_key) then
        self.velocity = Vector:create(0, final_speed * -1)
    elseif love.keyboard.isDown(self.down_key) then
        self.velocity = Vector:create(0, final_speed)
    else
        self.velocity = Vector:create(0, 0)
    end
end

function Player:__ai__()
    if timer > 0.2 then
        if (self.location.y > game.ball.location.y+game.ball.sizes[2]) or (self.location.y < game.ball.location.y-game.ball.sizes[2]) then
            self.point = math.floor(math.random(game.ball.location.y-game.ball.sizes[2]/2, game.ball.location.y+game.ball.sizes[2]/2))
            timer = 0
        end
    end
    local point2 = self.point - self.location.y
    if math.abs(point2) > game.speed*100 then
        final_speed = game.speed*game.boost
    else
        final_speed = game.speed
    end
    if point2 > 0 then
        self.velocity = Vector:create(0, final_speed)
    elseif point2 < 0 then
        self.velocity = Vector:create(0, final_speed * -1)
    else
        self.velocity = Vector:create(0, 0)
    end
end

function Player:update(dt)
    self.location = self.location + self.velocity*dt
    self.y = self.location.y - self.sizes["height"]/2
    if self.y < play_zone_height/10 + play_zone_y then
        self.y = play_zone_height/10 + play_zone_y
        self.location.y = play_zone_height/10 + play_zone_y + self.sizes["height"]/2
    elseif self.y > play_zone_height + play_zone_y - self.sizes["height"] - play_zone_height/10 then
        self.y = play_zone_height + play_zone_y - self.sizes["height"] - play_zone_height/10
        self.location.y = play_zone_height + play_zone_y - play_zone_height/10 - self.sizes["height"]/2
    end
    
    if self.ai then
        self:__ai__()
    else
        self:control()
    end
end

function Player:draw()
    -- love.graphics.rectangle("fill", self.x, self.y, self.sizes["width"], self.sizes["height"])
    love.graphics.draw(playerImg, self.location.x-self.sizes["width"]/2*self.side, self.location.y+self.sizes["height"]/2*self.side,
                        math.pi/(-2)*self.side, self.sizes["width"]/playerImg:getHeight()/widthScale*heightScale, self.sizes["height"]/playerImg:getWidth()*widthScale/heightScale)
end
