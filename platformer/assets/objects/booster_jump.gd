extends Area2D

@onready var my_animation = $booster_animation

const interact_label = "E"

var ready_to_interact = true
# Called when the node enters the scene tree for the first time.
func _ready():
	my_animation.play("idle")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_animation_finished():
	ready_to_interact = true
	my_animation.play("idle")

func interact(body):
	body.jump_charged = true
	body.player_animation.modulate = Color(0, 1, 1.5)
	ready_to_interact = false
	my_animation.play("charge")
