extends Area2D

@onready var my_animation = $door_animation

const interact_label = "E"

var ready_to_interact = true
# Called when the node enters the scene tree for the first time.
func _ready():
	my_animation.frame = 0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func interact(body):
	if body.has_key:
		ready_to_interact = false
		my_animation.play("idle")
	else:
		body.show_key_message()

func _on_animation_finished():
	get_tree().change_scene_to_file("res://level_menu.tscn")
