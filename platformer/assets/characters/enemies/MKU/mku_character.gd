extends CharacterBody2D

@onready var my_animation = $mku_animation
#@onready var detection_collision_right = $detection_area/detection_collision_right
#@onready var detection_collision_left = $detection_area/detection_collision_left
#@onready var attack_collision_right = $attack_area/attack_collision_right
#@onready var attack_collision_left = $attack_area/attack_collision_left
#@onready var wall_detection_right = $wall_detection_right
#@onready var wall_detection_left = $wall_detection_left
#@onready var stair_detection_right = $stair_detection_right
#@onready var stair_detection_left = $stair_detection_left

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

const speed_walk = 100
const speed_run = 200
const accel_walk = 2000  * (speed_walk/300.)
const accel_run = 2000  * (speed_run/300.)
const friction = 1500  * (speed_walk/300.)
const chase_time = 3.0
const attack_window_time = 0.05
const damage = 2

func _ready():
	pass

func _process(delta):
	pass


var chase_mode = 0
var chase_timer = 0.0
var attack_ready = false
var attack = false
var attack_window_timer = 0.0
var attack_window_flag = true
var player = null
var look_right = false
var patrol_right = false
var patrol_last_pos
var path_last_pos
var cur_state = "idle"
var cur_velocity
var speed = speed_walk
var accel = accel_walk
var attacks = []
func _physics_process(delta):
	#gravity
	if not is_on_floor():
		velocity.y += gravity * delta
	if cur_state != "die":
		if cur_state != "attack":
			#rotation
			if velocity.x > 0:
				my_animation.flip_h = true
				#detection_collision_left.disabled = true
				#detection_collision_right.disabled = false
				look_right = true
			elif velocity.x < 0:
				my_animation.flip_h = false
				#detection_collision_left.disabled = false
				#detection_collision_right.disabled = true
				look_right = false
			else:
				cur_state = "idle"
				my_animation.play("idle")

			if chase_mode>0:
				cur_velocity = accel*delta
				cur_state = "walk"
				my_animation.play("walk")
				if player != null:
					if player.position.x > self.global_position.x:
						velocity.x += cur_velocity
					else:
						velocity.x -= cur_velocity
					velocity.x = velocity.limit_length(speed).x
				if attacks.size() > 0:
					cur_state = "shoot"
					velocity.x = 0
					attack = true
					my_animation.play("shoot")
				
			#if stair_detection_right.is_colliding():
				#if not wall_detection_right.is_colliding():
					#self.global_position.y = stair_detection_right.get_collision_point().y - 1
					#self.global_position.x += 2
			#if stair_detection_left.is_colliding():
				#if not wall_detection_left.is_colliding():
					#self.global_position.y = stair_detection_left.get_collision_point().y - 1
					#self.global_position.x -= 2

			#cansel chase
			if chase_mode == 2:
				chase_timer += delta
				if chase_timer > chase_time:
					chase_mode = 0
					speed = speed_walk
					accel = accel_walk
					player = null
					attacks.clear()
					#path_last_pos = nav2d.get_simple_path(position, patrol_last_pos)
				
		else:
			if (my_animation.frame in [7, 9, 11]) and attack:
				if attack_window_flag:
					if look_right:
						pass
					attack_window_flag = false
				else:
					attack_window_timer += delta
					if attack_window_timer > attack_window_time:
						if look_right:
							pass
						attack_window_timer = 0.0
						attack_window_flag = true
						attack = false
	move_and_slide()


func _on_detection_body_entered(body):
	if cur_state != "die":
		if body.is_in_group("player"):
			player = body
			chase_mode = 1
			chase_timer = 0.0
			speed = speed_run
			accel = accel_run


func _on_detection_body_exited(body):
	if cur_state != "die":
		if body.is_in_group("player"):
			chase_mode = 2


func _on_attack_body_entered(body):
	if cur_state != "die":
		if body.is_in_group("can_be_hitted"):
			velocity.x = 0
			cur_state = "attack"
			attack = true
			my_animation.play("attack")
			attacks.push_back(body)
		
func _on_attack_body_exited(body):
	if cur_state != "die":
		for i in range(attacks.size() -1, -1, -1):
			if attacks[i] == body:
				attacks.remove_at(i)

#проверка окончания анимации удара
func _on_animation_finished():
	if cur_state == "attack":
		cur_state = "idle"

func _on_hit_body_entered(body):
	if cur_state != "die":
		if body.is_in_group("can_be_hitted"):
			if await body.take_damage(damage, (self.global_position - body.global_position).normalized()):
				chase_mode = 0
				speed = speed_walk
				accel = accel_walk
				player = null
				attacks.clear()

func take_damage(dmg):
	if cur_state != "die":
		if dmg>1:
			cur_state = "die"
			my_animation.play("die")
			self.set_collision_layer_value(32, true)
			self.set_collision_layer_value(3, false)
			self.set_collision_mask_value(2, false)
