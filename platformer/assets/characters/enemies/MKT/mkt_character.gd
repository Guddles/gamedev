extends CharacterBody2D

@onready var my_animation = $mkt_animation
@onready var detection_ray = $mkt_ray

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

const chase_time = 3.0
const damage = 2

func _ready():
	pass

func _process(delta):
	pass


var chase_timer = 0.0
var attack_ready = true
var attack = false
var cur_state = "idle"
var attacks = []
func _physics_process(delta):
	if cur_state != "die":
		await check_for_enemy()
		if cur_state != "shoot":
			cur_state = "idle"
			my_animation.play("idle")
		else:
			shoot()
			chase_timer += delta
			check_for_stop()


func check_for_enemy():
	if detection_ray.is_colliding():
		if detection_ray.get_collider().name == "player_character":
			cur_state = "shoot"
			return true

func shoot():
	if my_animation.animation != "shoot":
		my_animation.play("shoot")
	if (my_animation.frame > 0) and attack_ready:
		#shooting
		attack_ready = false
	if (my_animation.frame < 1):
		attack_ready = true

func check_for_stop():
	if check_for_enemy():
		chase_timer = 0
	elif chase_timer > chase_time:
		cur_state = "idle"

func take_damage(dmg):
	if cur_state != "die":
		cur_state = "die"
		my_animation.play("die")
		self.set_collision_layer_value(32, true)
		self.set_collision_layer_value(3, false)
		self.set_collision_mask_value(2, false)
		self.set_collision_mask_value(5, false)
